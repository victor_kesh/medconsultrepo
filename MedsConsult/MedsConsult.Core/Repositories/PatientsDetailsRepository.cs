﻿using Laxem.Portable.Helpers;
using MedsConsult.Core.Model;
using MedsConsult.Portable.Interfaces.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedsConsult.Core.Repositories
{
    public class PatientsDetailsRepository : BaseRepository<PatientsDetail> 
    {
        public OperationStatus AddPatientDetail(IPatientDetail account)
        {
            return Add((PatientsDetail)account);
        }

        public OperationStatus UpdatePatientDetail(IPatientDetail account)
        {
            return Update((PatientsDetail)account);
        }

        public OperationStatus DeleteAccount(IPatientDetail account)
        {
            return Remove((PatientsDetail)account); 
        }
    }
}
