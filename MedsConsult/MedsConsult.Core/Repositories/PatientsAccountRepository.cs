﻿using Laxem.Portable.Helpers;
using MedsConsult.Core.Model;
using MedsConsult.Portable.Interfaces.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedsConsult.Core.Repositories
{
    public class PatientsAccountRepository : BaseRepository<PatientsAccount> 
    {
        public OperationStatus AddPatientAccount(IPatientsAccount account)
        {
            return Add((PatientsAccount)account);
        }

        public OperationStatus UpdateAccount(IPatientsAccount account)
        {
            return Update((PatientsAccount)account);
        }

        public OperationStatus DeleteAccount(IPatientsAccount account)
        {
            return Remove((PatientsAccount)account);
        }
    }
}
