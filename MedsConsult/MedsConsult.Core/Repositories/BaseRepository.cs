﻿using Laxem.Portable.Helpers;
using Laxem.Portable.Interfaces;
using MedsConsult.Core.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using EntityState = System.Data.Entity.EntityState;


namespace MedsConsult.Core.Repositories
{
    public abstract class BaseRepository<T> where T : class
    {
        private DbContext _context;
        private OperationStatus _opState;
        public BaseRepository()
        {
            _context = new MedsConsultEntities();
            _opState = OperationStatus.CreateOpStatus();
        }

        internal virtual OperationStatus Add(T Entity)
        {
            try
            {
                var dbSet = _context.Set<T>();
                dbSet.Add(Entity);
                _context.SaveChanges();
                _opState.State = OperationStatus.Success;
                return _opState;
            }
            catch (Exception ex)
            {
                _opState.State = OperationStatus.Fail;
                _opState.ErrorMessage = ex.Message;
                return _opState;
            }

        }

        internal virtual OperationStatus AddAndGetId(T Entity)
        {
            try
            {
                long tempId = 0;
                var dbSet = _context.Set<T>();

                dbSet.Add(Entity);
                _context.SaveChanges();
                _opState.State = OperationStatus.Success;
                _opState.Result = tempId;

                var _id = Entity.GetType().GetProperty("ID").GetValue(Entity, null);
                if (long.TryParse(_id.ToString(), out tempId))
                {
                    _opState.State = OperationStatus.Success;
                    _opState.Result = _id;
                }

                return _opState;
            }
            catch (Exception ex)
            {
                _opState.State = OperationStatus.Fail;
                _opState.ErrorMessage = ex.Message;
                _opState.Result = 0;
                return _opState;
            }



        }

        internal virtual OperationStatus AddMany(List<T> items)
        {
            try
            {
                foreach (T item in items)
                {
                    _context.Entry(item).State = EntityState.Added;
                }
                _context.SaveChanges();
                _opState.State = OperationStatus.Success;
                return _opState;
            }
            catch (Exception ex)
            {
                _opState.State = OperationStatus.Fail;
                _opState.ErrorMessage = ex.Message;
                return _opState;
            }

        }

        internal virtual List<T> GetList(Func<T, bool> where, params Expression<Func<T, object>>[] navigationProperties)
        {
            List<T> list;
            IQueryable<T> dbQuery = _context.Set<T>();
            //Apply eager loading
            foreach (Expression<Func<T, object>> navigationProperty in navigationProperties)
                dbQuery = dbQuery.Include<T, object>(navigationProperty);
            list = dbQuery
                 .AsNoTracking()
                 .Where(where)
                 .ToList<T>();
            return list;
        }
        internal virtual List<T> GetList(params Expression<Func<T, object>>[] navigationProperties)
        {
            List<T> list;
            IQueryable<T> dbQuery = _context.Set<T>();
            //Apply eager loading
            foreach (Expression<Func<T, object>> navigationProperty in navigationProperties)
                dbQuery = dbQuery.Include<T, object>(navigationProperty);
            list = dbQuery
                 .AsNoTracking()
                 .ToList<T>();
            return list;
        }

        internal virtual OperationStatus Remove(T entity)
        {
            try
            {
                _context.Entry(entity).State = EntityState.Deleted;
                _context.SaveChanges();
                _opState.State = OperationStatus.Success;
                return _opState;
            }
            catch (Exception ex)
            {
                _opState.State = OperationStatus.Fail;
                _opState.ErrorMessage = ex.Message;
                return _opState;
            }
        }

        internal virtual OperationStatus Update(T entity)
        {
            try
            {
                _context.Entry(entity).State = EntityState.Modified;
                _context.SaveChanges();
                _opState.State = OperationStatus.Success;
                return _opState;
            }
            catch (Exception ex)
            {
                _opState.State = OperationStatus.Fail;
                _opState.ErrorMessage = ex.Message;
                return _opState;
            }
        }

    }
}
