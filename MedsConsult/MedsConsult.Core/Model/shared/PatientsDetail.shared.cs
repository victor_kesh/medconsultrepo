﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MedsConsult.Portable.Interfaces.Models;
namespace MedsConsult.Core.Model
{
    public partial class PatientsDetail : IPatientDetail
    {
        int IPatientDetail.Age
        {
            get { return DateTime.Now.Year - DOB.Year; }
        }


        string IPatientDetail.FullName
        {
            get
            {
                return $"{FirstName} {MiddleName} {LastName}";
            }
        }


    }
}