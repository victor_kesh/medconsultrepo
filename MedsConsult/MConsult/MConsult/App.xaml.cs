﻿using MConsult.Views.PatientDetail;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MConsult
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new PatientDetailView();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
