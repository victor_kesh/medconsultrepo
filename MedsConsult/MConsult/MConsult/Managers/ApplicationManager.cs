﻿using Laxem.Portable.Interfaces;
using Laxem.Portable.Managers;

using System;
using System.Configuration;
using System.Collections.Generic;
using System.Text;

using Unity;

namespace MConsult.Managers
{
    public class ApplicationManager : BaseApplicationManager
    {
        private IUserSession _userSession;
        public ApplicationManager(IUserSession userSession)
        {
            _userSession = userSession;
        }

        public override IUserSession UserSession
        {
            get
            {
                return _userSession;
            }
        }

        public override int AllowedLoginTrials
        {
            get
            {
                return 3;
            }
        }


        public override string ApplicationVersion
        {
            get
            {
                return string.Empty;
            }
        }
        public override string ApplicationBaseUrl
        {
            get
            {
                return "https://localhost:44344/";
                // ConfigurationManager.AppSettings["ApplicationBaseUrl"];
            }
        }

        public override int ApplicationId
        {
            get; 
        }
    }
}
