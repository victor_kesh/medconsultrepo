﻿using Laxem.Portable.Interfaces;

using System;
using System.Collections.Generic;
using System.Text;

namespace MConsult.Managers
{
    public class UserSession : IUserSession
    {
        private ICommonUserAccount _currentUser;
        public ICommonUserAccount CurrentUser
        {
            get
            {
                return _currentUser;
            }
            set
            {
                _currentUser = value;
            }
        }

        private DateTimeOffset _loginDateTime;
        public DateTimeOffset LoginDateTime
        {
            get
            {
                return _loginDateTime;
            }
            set
            {
                _loginDateTime = value;
            }
        }

        private bool _isSessionActive;
        public bool IsSessionActive
        {
            get
            {
                return _isSessionActive;
            }
            set
            {
                _isSessionActive = value;
            }
        }

        int _currentUserLoginTrials;
        public int CurrentUserLoginTrials
        {
            get
            {
                return _currentUserLoginTrials;
            }
            set
            {
                _currentUserLoginTrials = value;
            }
        }

        public int SessionTimeOutInMinutes
        {
            get;
        }
    }
}
