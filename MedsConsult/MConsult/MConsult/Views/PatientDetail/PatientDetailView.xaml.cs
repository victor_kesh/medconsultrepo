﻿using MConsult.Views.PatientDetail.ViewModels;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MConsult.Views.PatientDetail
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PatientDetailView : ContentPage
    {
        PatientDetailViewModel _viewModel;
        public PatientDetailView()
        {
            InitializeComponent();
        }
        protected async override void OnAppearing()
        {
            if (BindingContext == null)
                await App.Current.MainPage.DisplayAlert("Error", $"{nameof(PatientDetailViewModel)} cannot be null", "Null View Model", "OK");
            _viewModel = (PatientDetailViewModel)BindingContext;
            _viewModel.OnViewReady();
      
        }
    }
}