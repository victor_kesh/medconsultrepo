﻿using Laxem.Portable.Interfaces;
using Laxem.SimpleMvvmToolkit;
using Laxem.SimpleMvvmToolkit.ViewModel;

using MConsult.Managers;
using MConsult.Wrappers;

using MedsConsult.Portable.DTOs;
using MedsConsult.Portable.Interfaces.Models;

using RestSharp;

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Input;

using Unity;

namespace MConsult.Views.PatientDetail.ViewModels
{
    public class PatientDetailViewModel : ViewModelBase<PatientsDetailDTO>
    {
        IApplicationManager _applicationManager;
        IUnityContainer _container;
        #region initialisation
        public PatientDetailViewModel()
        {
        }
        public override void OnViewReady()
        {
            SetupIOC();
        }
        private void SetupIOC()
        {
            _container = UnityWrapper.GetContainer();
            //initialise user session
            var useSession = new UserSession
            {
                IsSessionActive = true
            };

            //initialise user session 
            var applicationManager = new ApplicationManager(useSession);
            _container.RegisterInstance<IApplicationManager>(applicationManager);
        }

        #endregion
        #region properties 
        public List<string> GenderList
        {
            get
            {
                return new List<string> { "Male", "Female" };
            }
        }
        private string _gender;
        public string Gender
        {
            get
            {
                return _gender;
            }
            set
            {
                _gender = value;
                NotifyPropertyChanged(nameof(Gender));
            }
        }
        private string _mail;
        public string Mail
        {
            get
            {
                return _mail;
            }
            set
            {
                _mail = value;
                NotifyPropertyChanged(nameof(Mail));
            }
        }


        private string _phoneNumber;
        public string PhoneNumber
        {
            get
            {
                return $"+254{_phoneNumber}";
            }
            set
            {
                _phoneNumber = value;
                NotifyPropertyChanged(nameof(PhoneNumber));
            }
        }

        public DateTime CurrentDate
        {
            get
            {
                return DateTime.Today;
            }
        }

        private bool _hasAcceptedConditions = false;
        public bool HasAcceptedConditions
        {
            get
            {
                return _hasAcceptedConditions;
            }
            set
            {
                _hasAcceptedConditions = value;
                NotifyPropertyChanged(nameof(HasAcceptedConditions));
            }
        }
        #endregion

        #region commands 
        private ICommand _saveDetailsCommand;
        public ICommand SaveDetailsCommand
        {
            get
            {
                return _saveDetailsCommand ??
                       (_saveDetailsCommand =
                        new DelegateCommand(SaveAccount, () => true));
            }
        }

        #endregion

        #region methods
        async void SaveAccount()
        {
            _applicationManager = _container.Resolve<IApplicationManager>();
            string url = $"{_applicationManager.ApplicationBaseUrl}/api/PatientsAccount/CreatePatientAccount";
            var response = await Task.Run(() =>
            {
                return _applicationManager.RestClient.SendRequest(Model, url, Method.POST);
            });
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
            }
            else
            {
            }
        }

        #endregion
    }
}
