﻿using Laxem.Portable.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace MConsult.Views.Login.ViewModels
{
    public class LoginViewModel
    {
        public LoginViewModel()
        { 
        
        
        }

        private ICommonUserAccount _userAccount;
        public ICommonUserAccount UserAccount
        {
            get
            {
                return _userAccount;
            }
            set
            {
                _userAccount = value;
                //NotifyPropertyChanged(nameof(UserAccount));
            }

        }
    }
}
