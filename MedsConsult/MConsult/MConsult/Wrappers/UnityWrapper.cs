﻿using System;
using System.Collections.Generic;
using System.Text;

using Unity;

namespace MConsult.Wrappers
{
    public class UnityWrapper
    {
        private static UnityContainer _instance;
        public static IUnityContainer GetContainer()
        {
            if (_instance == null)
                _instance = new UnityContainer();
            return _instance;
        }

    }
}
