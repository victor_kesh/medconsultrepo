﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedsConsult.Portable.Tokens
{
    public static class Salts
    {
        public const string AndroidMobileApp = "3#@77(9g7%9!";
        public const string WebApp = "9$*grH0>099)";
    }
}
