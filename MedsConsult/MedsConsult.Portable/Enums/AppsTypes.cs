﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedsConsult.Portable.Enums
{
    public enum AppsTypes
    {
        AndroidMobileApp = 1,
        WebApp = 2,
        IOSMobileApp = 3,
    }
}
