﻿using MedsConsult.Portable.Interfaces.Models;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedsConsult.Portable.DTOs
{
    public class PatientsDetailDTO : DTOBase<PatientsDetailDTO>, IPatientDetail
    {
        public long ID
        {
            get; set;
        }
        public string IdNumber
        {
            get; set;
        }
        public string FirstName
        {
            get; set;
        }
        public string MiddleName
        {
            get; set;
        }
        public string LastName
        {
            get; set;
        }
        public DateTime DOB
        {
            get; set;
        }
        public string Gender
        {
            get; set;
        }
        public int Age
        {
            get;
        }
        public string FullName
        {
            get;
        }


    }
}
