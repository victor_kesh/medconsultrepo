﻿using Laxem.Portable.Interfaces;
using MedsConsult.Portable.Interfaces.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MedsConsult.Portable.DTOs
{
    public class PatientAccountDTO:IPatientsAccount, ICommonUserAccount
    {
       public long ID { get; set; }
        public string UserName { get; set; }
        public string UserPwd { get; set; }
        public bool IsDisabled { get; set; }
        public string AccountState { get; set; }
        public DateTimeOffset DateCreated { get; set; }
        public string Email { get; set; }
        public  string TelNo { get; set; }
        int? ICommonUserAccount.GroupId { get; set; }
        string ICommonUserAccount.FirstName { get; set; }
        string ICommonUserAccount.OtherNames { get; set; }
        string ICommonUserAccount.IDNo { get; set; }
        string ICommonUserAccount.Mail { get; set; }
        string ICommonUserAccount.AccountPwd { get; set; }
        bool ICommonUserAccount.AccountActivated { get; set; }
        string ICommonUserAccount.Active { get; set; }
    }
}
