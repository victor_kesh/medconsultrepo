﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedsConsult.Portable.Interfaces.Models
{
    public interface IPatientsContact
    {
       long ID { get; set; }
       long PatientId { get; set; }
       string ContactType { get; set; }
       string Contact { get; set; }
       string IsPrimaryContact { get; set; }
    }
}
