﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedsConsult.Portable.Interfaces.Models
{
    public interface IPractitionersDetail
    {
        long ID { get; set; }
        string Title { get; set; }
        string FirstName { get; set; }
        string OtheNames { get; set; }
        string Gender { get; set; }
        string IDNumber { get; set; }
        string Email { get; set; }
        DateTimeOffset DateCreated { get; set; }
    }
}
