﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedsConsult.Portable.Interfaces.Models
{
    public interface IPatientsDetail
    {
        long ID { get; set; }
        string IdNumber { get; set; }
        string FirstName { get; set; }
        string MiddleName { get; set; }
        string LastName { get; set; }
        DateTime DOB { get; set; }
        string Gender { get; set; }
        int Age { get;}
        string FullName { get; }
    }
}
