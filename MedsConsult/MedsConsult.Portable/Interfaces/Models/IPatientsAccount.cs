﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedsConsult.Portable.Interfaces.Models
{
    public interface IPatientsAccount
    {
        long ID { get; set; }
        string UserName { get; set; }
        string UserPwd { get; set; }
        bool IsDisabled { get; set; }
        string AccountState { get; set; }
        DateTimeOffset DateCreated { get; set; }
        string Email { get; set; }
        string TelNo { get; set; }
    }
}
