﻿using Laxem.Portable.Interfaces;
using Laxem.Portable.Managers;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MedsConsultAPI.Managers
{
    public class ApplicationManager : BaseApplicationManager
    {

        private IUserSession _userSession;
        public ApplicationManager(IUserSession userSession)
        {
            _userSession = userSession;
        }

        public override IUserSession UserSession
        {
            get
            {
                return _userSession;
            }
        }

        public override int AllowedLoginTrials
        {
            get
            {
                return 3;
            }
        }

        public override string ApplicationName
        {
            get
            {
                return string.Empty;
            }
        }

        public override string ApplicationVersion
        {
            get
            {
                return string.Empty;
            }
        }
        public override string ApplicationBaseUrl
        {
            get
            {
                return string.Empty;
            }
        }

    }
}