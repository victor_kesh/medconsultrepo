﻿using Laxem.Portable.Helpers;
using Laxem.Portable.Interfaces;

using MedsConsult.Core.Repositories;
using MedsConsult.Portable.DTOs;
using MedsConsultAPI.Factories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MedsConsultAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PatientsAccountController : ControllerBase
    {
        PatientsAccountRepository _repository;

        PatientsAccountController()
        {
            _repository = RepositoriesFactory.CreatePatientAccountRepository();
        }

        [HttpPost]
        public ICommonOperationStatus CreatePatientAccount([FromBody] PatientAccountDTO account)
        {
            return _repository.UpdateAccount(account);
        }

        [HttpGet]
        public ICommonOperationStatus UpdatePatientAccount(PatientAccountDTO account)
        {
            return _repository.UpdateAccount(account);
        }
    }
}
