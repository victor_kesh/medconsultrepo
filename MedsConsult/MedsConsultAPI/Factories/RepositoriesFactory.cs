﻿using MedsConsult.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MedsConsultAPI.Factories
{
    public class RepositoriesFactory
    {
        public static PatientsAccountRepository CreatePatientAccountRepository()
        {
            return new PatientsAccountRepository();
        }

        public static PatientsDetailsRepository CreatePatientsDetailsRepository()
        {
            return new PatientsDetailsRepository();
        }
    }
}
